﻿using AutoMapper;
using MovieAPI.Models;
using MovieAPI.Models.DTOs.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {

            CreateMap<Character, CharacterReadDTO>()
                .ForMember(m => m.Movies, opt => opt.MapFrom(a => a.Movies.Select(m => m.MovieId).ToArray()))
                .ReverseMap();

            CreateMap<Character, CharacterEditDTO>()
                .ForMember(adto => adto.MovieId, m => m.MapFrom(m => m.CharacterId)).ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
            .ReverseMap();
                
        }
    }
}
