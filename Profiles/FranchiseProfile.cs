﻿using AutoMapper;
using MovieAPI.Models;
using MovieAPI.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(adto => adto.Movies, opt => opt
                .MapFrom(a => a.Movies
                .Select(m => m.MovieId).ToList()))
                .ReverseMap();
        

            CreateMap<Franchise, FranchiseEditDTO>()
                .ForMember(m => m.MovieId, opt => opt.MapFrom(m =>m.FranchiseId)).ReverseMap();


            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
        }      
    }
}
