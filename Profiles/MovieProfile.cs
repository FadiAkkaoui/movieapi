﻿using AutoMapper;
using MovieAPI.Models;
using MovieAPI.Models.DTOs.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
            .ForMember(mdto => mdto.Franchise, opt => opt.MapFrom(m => m.FranchiseId))
            .ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.Characters.Select(m => m.CharacterId)))
            .ReverseMap();

            CreateMap<Movie, MovieEditDTO>()
           .ForMember(adto => adto.FranchiseId, opt => opt
           .MapFrom(a => a.FranchiseId)).ReverseMap();


            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(adto => adto.FranchiseId, opt => opt
           .MapFrom(a => a.FranchiseId))
                .ReverseMap();

           // .ForMember(adto => adto.MovieId, opt => opt
           //.MapFrom(a => a.Movie.Select(m => m.MovieId)))
           //     .ReverseMap();

        }
    }
}
