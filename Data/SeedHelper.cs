﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Data
{
    public class SeedHelper
    {
        public static IEnumerable<Franchise> GetFransciseSeeds()
        {
            IEnumerable<Franchise> seedFranchise = new List<Franchise>()
            {
                new Franchise
                {
                    FranchiseId = 1,
                    Name = "DC Extended Universe",
                    Description = "A collection of interconnected films and other media based on characters that appear in publications by DC Comics."
                    
                },
                new Franchise
                {
                    FranchiseId = 2,
                    Name = "Marvel Cinematic Universe",
                    Description = "The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics."
                }

            };
            return seedFranchise;
        }
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> seedMovie = new List<Movie>()
            {
                new Movie
                {
                    MovieId = 1,
                    MovieTitle = "Man of Steel",
                    Genre = "Action, Adventure",
                    ReleaseYear = 2013,
                    Director = "Zack Snyder",
                    Picture = "https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_.jpg",
                    Trailer = "https://www.youtube.com/watch?v=wArmHSPIvlQ",
                    FranchiseId = 1
                    
                },
                new Movie
                {
                    MovieId = 2,
                    MovieTitle = "Wonder Woman",
                    Genre = "Action, Adventure",
                    ReleaseYear = 2017,
                    Director = "Patty Jenkins",
                    Picture = "https://tse3.mm.bing.net/th/id/OIP.XpjC_TAfl6SIZD57u1LZcwHaFI?pid=ImgDet&rs=1",
                    Trailer = "https://www.youtube.com/watch?v=u1NlmFa0-68",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 3,
                    MovieTitle = "Avengers: Infinity War",
                    Genre = "Action, Adventure, Sci-Fi",
                    ReleaseYear = 2015,
                    Director = "Anthony Russo, Joe Russo",
                    Picture = "https://image.tmdb.org/t/p/w1280/xin9oDG1Cl0siVYYHMsxdRyESj2.jpg",
                    Trailer = "https://www.youtube.com/watch?v=f7Isvbe9SW4",
                    FranchiseId = 2
                }
            };
            return seedMovie;
        }
        public static IEnumerable<Character> GetCharacterSeeds()
        {
                IEnumerable<Character> seedCharacter = new List<Character>()
                {
                new Character
                {
                    CharacterId = 1,
                    FullName = "Henry Cavill",
                    Alias = "Super Man",
                    Gender = "Man",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/3/30/Henry_Cavill_%2848417913146%29_%28cropped%29.jpg",
                    
                },
                new Character
                {
                    CharacterId = 2,
                    FullName = "Amy Adams",
                    Alias = "Lois Lane",
                    Gender = "Woman",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Amy_Adams_UK_Nocturnal_Animals_Premiere_%28cropped%29.jpg/800px-Amy_Adams_UK_Nocturnal_Animals_Premiere_%28cropped%29.jpg"
                },
                new Character
                {
                    CharacterId = 3,
                    FullName = "Gal Gadot",
                    Alias = "Wonder Woman",
                    Gender = "Woman",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Gal_Gadot_cropped_lighting_corrected.jpg/800px-Gal_Gadot_cropped_lighting_corrected.jpg"
                },
                 new Character
                {
                    CharacterId = 4,
                    FullName = "Chris Hemsworth",
                    Alias = "Thor",
                    Gender = "Man",
                    Picture = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSZ6TeDFGGXiuKR36W9VBw93YeRuOf_-eaDmfqJ1InJONlGKnqV"
                }

                };
            return seedCharacter;
        }
      
    } 
}

       
