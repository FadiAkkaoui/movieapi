﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Data
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters{ get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFransciseSeeds());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());


            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string,object>>(
                "CharacterMovie",
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                I => I.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 2, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 4 }
                        );
                });

        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer("Data Source=localhost\\SQLEXPRESS;Initial Catalog=MovieApiDb;Integrated Security=True");
        }

        public MovieDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
