﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Data;
using MovieAPI.Models;
using MovieAPI.Models.DTOs.Franchise;

namespace MovieAPI.Controllers
{
    [Route("api/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

       /// <summary>
       /// Get all franchises
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.Include(m => m.Movies).ToListAsync());
        }

        /// <summary>
        /// Get a specific franchase by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = _mapper.Map<FranchiseReadDTO>(await _context.Franchises.FindAsync(id));

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }
        /// <summary>
        /// Get movies in franchase by franchise id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet ("{id}/GetMovies")]
        public async Task<ActionResult<IEnumerable<Movie>>> GetAllMovies(int id)
        {

            var movies = await _context.Movies
                .Where(m => m.FranchiseId == id)
                .ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            return movies;

        }
        /// <summary>
        /// Update a franchise by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchisedto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchisedto)
        {
            if (id != franchisedto.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchisedto);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new franchise.
        /// </summary>
        /// <param name="franchisedto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchisedto)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchisedto);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delete a franchise by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Check if a franchise exists by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

        //[HttpPost("{movieId}")]
        //public async Task<ActionResult> PostFranchiseWithMovies(Franchise franchise, int MovieId)
        //{
        //    var franchiseMovie = await _context.Franchises.Include(fr => fr.Movies).Where(m => m.FranchiseId == fr.MovieId);
        //    var franchises = await _context.Movies.FindAsync(MovieId);
        //    franchiseMovie.Movies.Add(franchises);

        //    await _context.SaveChangesAsync();


        //    return CreatedAtAction("GetFranchise", new { id = franchise.FranchiseId }, franchise);
        //}

        /// <summary>
        /// Update a movie by franchise id. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateCharacterFranchise(int id, int[] movies)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise franshiseToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.FranchiseId == id)
                .FirstAsync();

            List<Movie> movie = new();
            foreach (int movId in movies)
            {
                Movie mov = await _context.Movies.FindAsync(movId);
                if (mov == null)
                    return BadRequest("Character doesnt exist");
                movie.Add(mov);

            }
            franshiseToUpdateMovies.Movies = movie;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
