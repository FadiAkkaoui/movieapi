﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        [MaxLength(400)]
        public string MovieTitle { get; set; }
        [MaxLength(400)]
        public string Genre { get; set; }
        [Range(2, 4, ErrorMessage = "Value must be between 2 to 4")]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        [Url]
        public string Picture { get; set; }
        [Url]
        public string Trailer { get; set; }
        //relationships
        public virtual ICollection<Character> Characters { get; set; }
        public virtual int FranchiseId { get; set; }
        public virtual Franchise Franchise { get; set; }
    }
}
