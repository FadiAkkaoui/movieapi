﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [Required]
        [MaxLength (400)]
        public string Name { get; set; }
        public string Description { get; set; }
        //realtionships
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
