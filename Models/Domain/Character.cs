﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(400)]
        public string FullName { get; set; }
        [MaxLength(400)]
        public string Alias { get; set; }
        public string Gender { get; set; }
        [Url]
        public string Picture { get; set; }
        //relationships
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
