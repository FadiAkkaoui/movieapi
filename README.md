# Pokemontrainer

This is a MovieCharacter assignment buildt with .NET Core API, where you can get data with stored franchises, with related movies and characters related to the movie. SQL server was used to build the database.  

This project was generated with .NET 5.


## Install

```
https://gitlab.com/FadiAkkaoui/movieapi.git
cd movieapi
```

## Usage

```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=localhost\\SQLEXPRESS;Initial Catalog=MovieApiDb;trusted_connection=true;Integrated Security=True"
  }
}
```

## Contributors
@FadiAkkaoui and @betielyohannes

## Contributing

No contributions allowed.


