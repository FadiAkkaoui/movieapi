﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchiiseAsync();
        public Task<Franchise> GetSpecificMovieAsync(int id);
        public Task<IEnumerable<Movie>> GetAllMoviesAsync(int id);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
        public Task UpdateFranchiseMovieAsync(int id, List<int> movies);
    }
}
