﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    /// <summary>
    /// This service is essentially a repository, they are interchangable, still follows the same pattern.
    /// </summary>
    public interface ICharacterService
    {
        // We can include a SaveChangesAsync method to have full control over EF, some implementaitons do this.
        // I am not for simplicity sake. Its technically more flexible.
        public Task<Character> AddCharacterAsync(Character character);
        public Task<Character> GetSpecificCharacterAsync(int id);
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        public Task UpdateCharacterAsync(Character character);
        public bool CharacterExists(int id);
        public Task DeleteCharacterAsync(int id);
    }
}
