﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpecificMovieAsync(int id);
        public Task UpdateMovieAsync(Movie movie);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
        public Task UpdateMovieCharactersAsync(int id, List<int> characters);
        public Task<IEnumerable<Character>> GetAllCharactersAsync(int id);

    }
}
