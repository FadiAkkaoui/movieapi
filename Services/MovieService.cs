﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Data;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
        public async Task UpdateMovieCharactersAsync(int id, List<int> characters)
        {
            Movie movieToUpdateCerts = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == id)
                .FirstAsync();

            // Loop through characters, try and assign to movie
            List<Character> character = new();
            foreach (int charId in characters)
            {
                Character chara = await _context.Characters.FindAsync(charId);
                if (chara == null)
                    throw new KeyNotFoundException();
                character.Add(chara);
            }
            movieToUpdateCerts.Characters = character;
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Character>> GetAllCharactersAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.MovieId == id)
                .SelectMany(m => m.Characters)
                .ToListAsync();
        }
    }
}
