﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Data;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Franchise>> GetAllFranchiiseAsync()
        {
            return await _context.Franchises
                .Include(m => m.Movies)
                .ToListAsync();
        }
        public async Task<Franchise> GetSpecificMovieAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync(int id)
        {
            return await _context.Movies
                .Include(m => m.FranchiseId)
                .ToListAsync();
        }
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
        public async Task UpdateFranchiseMovieAsync(int id, List<int> movies)
        {
            Franchise franchiseToUpdateCerts = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.FranchiseId == id)
                .FirstAsync();

            // Loop through characters, try and assign to movie
            List<Movie> movies1 = new();
            foreach (int movieId in movies)
            {
                Movie mov = await _context.Movies.FindAsync(movieId);
                if (mov == null)
                    throw new KeyNotFoundException();
                movies1.Add(mov);
            }
            franchiseToUpdateCerts.Movies = movies1;
            await _context.SaveChangesAsync();
        }
    }
}
